#pragma once

#include <stdexcept>
#include "ListSorter.h"
#include <cmath>

template<class T>
class ListSorterQuick2 : public ListSorter<T>
{
public:
	ListSorterQuick2(ListArray<T>& listToUse);
	virtual void sort();
	int partition(int first, int last);
	void sortR(int first, int last, int depth) throw(std::overflow_error);
	T pickPivot(int first, int last);
};

template<class T>
ListSorterQuick2<T>::ListSorterQuick2(ListArray<T>& listToUse) :
	ListSorter(listToUse)
{}

template<class T>
void ListSorterQuick2<T>::sort()
{

	sortR(0, list.size() - 1, 0);

}
// SOURCE: Modified code from CSC-161 class, Kevin Lundeen
template<class T>
void ListSorterQuick2<T>::sortR(int first, int last, int depth) throw(std::overflow_error)
{
	// base case -- stack overflow

	if (depth > 3500)
		throw std::overflow_error("Failed qsort--stack too deep");

	// base case -- empty list
	if (first > last)
		return;
	int pivotIndex = partition(first, last);
	sortR(first, pivotIndex - 1, depth + 1);
	sortR(pivotIndex + 1, last, depth + 1);

}
template<class T>
T ListSorterQuick2<T>::pickPivot(int first, int last)
{
	T pivot;
	T elementFirst = list.at(1);
	T elementMiddle = list.at((last - first) / 2);
	T elementLast = list.at(last);

	if (elementFirst >= elementLast && elementFirst <= elementMiddle || elementFirst <= elementLast && elementFirst >= elementMiddle)
		pivot = elementFirst;
	if (elementMiddle >= elementLast && elementMiddle <= elementFirst || elementMiddle <= elementLast && elementMiddle >= elementFirst)
		pivot = elementMiddle;
	if (elementLast >= elementFirst && elementLast <= elementMiddle || elementLast <= elementFirst && elementLast >= elementMiddle)
		pivot = elementLast;
	return pivot;
}
template<class T>
int ListSorterQuick2<T>::partition(int first, int last)
{	
	T pivot = pickPivot(first, last);
	int i = first;
	for (int j = first; j < last; j++)
		if (list.at(j) <= pivot)
			std::swap(list.at(i++), list.at(j));
	std::swap(list.at(i), list.at(last));
	return i;

}