#pragma once

#include <stdexcept>
#include "List.h"

template<class T>
class ListArray : public List<T>
{
private:
	T *data;
	int allocated; // The total amount of space
	int length; // The amount of filled space
public:
	ListArray(int sizeHint);
	virtual int size() const;
	int capacity() const override;
	virtual int push_back(const T& newEntry);
	virtual T& at(int position);
};

template<class T>
inline ListArray<T>::ListArray(int sizeHint) : List(sizeHint) {
	allocated = sizeHint; 
	data = new T[allocated];
	for (int i = 0; i < allocated; i++)
		data[i] = 0; // init to null values so that next empty spot can be found
	length = 0; // initialize length
}																 

template<class T>
int ListArray<T>::size() const {
	return length;
}

template<class T>
int ListArray<T>::capacity() const {
	return allocated;
}

template<class T>
int ListArray<T>::push_back(const T & newEntry)
{
	int emptySlot = length;
	if (emptySlot > allocated)
		throw std::out_of_range("exceeded capacity");
	data[emptySlot] = newEntry;
	length++;
	return emptySlot;
	
	// TODO: throw out_of_range exception if the capacity is exceeded
	// TODO: ? Find last 'null' element to know where to insert new item ?
}

template<class T>
T& ListArray<T>::at(int position) {
	if (position > length || position < 0)
		throw std::out_of_range("invalid position");
			return data[position];
}