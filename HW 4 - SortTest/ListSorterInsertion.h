#pragma once

#include "ListSorter.h"

template<class T>
class ListSorterInsertion : public ListSorter<T>
{
public:
	ListSorterInsertion(ListArray<T>& listToUse);
	virtual void sort();
};

template<class T>
ListSorterInsertion<T>::ListSorterInsertion(ListArray<T>& listToUse) :
	ListSorter(listToUse)
{}

template<class T>
void ListSorterInsertion<T>::sort()
{
	// NOTE: I wrote this myself without any help other than a visualization. I THINK it is an insertion sort. If not, I learned a lot by writing it all on my own.
	
	T temp; // temp holder for element that will be inserted
	T currentElement; // the index of the element we are sorting

	for (currentElement = 1; currentElement < list.size(); ++currentElement) 
	{
		if (list.at(currentElement) < list.at(currentElement - 1)) // if the element we are looking at is less than the element one to the left
		{
			temp = list.at(currentElement); // store the current element in a temp variable
			list.at(currentElement) = list.at(currentElement - 1); // move current element to the left
			list.at(currentElement - 1) = temp; // move other element to the right

			for (int i = currentElement; i > 0; i--) // now look at other elements on the left to see if it needs to be moved further to the left
				if (list.at(i) < list.at(i - 1)) 
				{
					temp = list.at(i); // store the current element in a temp variable
					list.at(i) = list.at(i - 1);
					list.at(i - 1) = temp;
				}
		}
	}
}