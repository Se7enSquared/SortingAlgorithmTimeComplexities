#pragma once

#include <stdexcept>
#include "ListSorter.h"

template<class T>
class ListSorterQuick : public ListSorter<T>
{
public:
	ListSorterQuick(ListArray<T>& listToUse);
	virtual void sort();
	int partition(int first, int last);
	void sortR(int first, int last, int depth) throw(std::overflow_error);
};

template<class T>
ListSorterQuick<T>::ListSorterQuick(ListArray<T>& listToUse) :
	ListSorter(listToUse)
{}

template<class T>
void ListSorterQuick<T>::sort()
{

	sortR(0, list.size() - 1, 0);
	
}
// SOURCE: Modified code from CSC-161 class, Kevin Lundeen
template<class T>
void ListSorterQuick<T>::sortR(int first, int last, int depth) throw(std::overflow_error)
{
	// base case -- stack overflow
	
		if (depth > 3500)
			throw std::overflow_error("Failed qsort--stack too deep");
	
		// base case -- empty list
		if (first > last)
			return;
		int pivotIndex = partition(first, last);
		sortR(first, pivotIndex - 1, depth + 1);
		sortR(pivotIndex + 1, last, depth + 1);
	
}
// SOURCE: D2L code from Kevin Lundeen
template<class T>
int ListSorterQuick<T>::partition(int first, int last)
{
	
	T pivot = list.at(last);
	int i = first;
	for (int j = first; j < last; j++)
		if (list.at(j) <= pivot)
			std::swap(list.at(i++), list.at(j));
	std::swap(list.at(i), list.at(last));
	return i;
	
}