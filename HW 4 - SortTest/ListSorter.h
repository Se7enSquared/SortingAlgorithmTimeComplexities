#pragma once
#include "ListArray.h"

template<class T>
class ListSorter
{
protected:
	ListArray<T>& list;
public:
	ListSorter(ListArray<T>& list);
	virtual void sort() = 0;
	void print(); // useful for debugging
};

template<class T>
ListSorter<T>::ListSorter(ListArray<T>& listToUse) :
	list(listToUse)
{}

template<class T>
void ListSorter<T>::print()
{
	for (int i = 1; i < list.size(); i++)
		std::cout << list.at(i) << " ";
	std::cout << std::endl;
}
