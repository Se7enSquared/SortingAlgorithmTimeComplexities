#pragma once

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "ListArray.h"
#include "ListSorterBubble.h"

template<class SorterClass>
class SortTester
{
public:
	SortTester();
	void runTests();
};

template<class SorterClass>
SortTester<SorterClass>::SortTester()
{
	srand(static_cast<unsigned int>(time(nullptr)));
}

template<class SorterClass>
void SortTester<SorterClass>::runTests()
{

	clock_t start, timeSearch;

	std::cout << "n\ttime" << std::endl;
	std::cout << "AVERAGE CASE" << std::endl;
	for (int n = 100; n <= 15000; n *= 2)
	{
		try {
		ListArray<int> list(n);
		for (int i = 0; i < n; i++)
			list.push_back(rand());
		start = clock();
		SorterClass sorter(list);
		sorter.sort();
		timeSearch = clock() - start;
		std::cout << n << "\t" << timeSearch << std::endl;
		}
		catch (std::overflow_error& e)
		{
			std::cout << n << "\t" << e.what() << std::endl;
		}
	}
	std::cout << "WORST CASE" << std::endl;
	for (int n = 100; n <= 15000; n *= 2)
	{
		try {
		ListArray<int> list2(n);
		for (int i = n; i > 0; i--)
			list2.push_back(i);
			start = clock();
			SorterClass sorter2(list2);
			sorter2.sort();
			timeSearch = clock() - start;
			std::cout << n << "\t" << timeSearch << std::endl;
			}
			catch (std::overflow_error& e)
			{
				std::cout << n << "\t" << e.what() << std::endl;
			}
			
	}
}