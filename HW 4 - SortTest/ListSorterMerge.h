#pragma once

#include <iostream>
#include <string>
#include "ListSorter.h"

template<class T>
class ListSorterMerge : public ListSorter<T>
{
protected:
	virtual void merge(int first, int mid, int last);
	virtual void mergeSort(int first, int last);
public:
	ListSorterMerge(ListArray<T>& listToUse);
	virtual void sort();
};

template<class T>
ListSorterMerge<T>::ListSorterMerge(ListArray<T>& listToUse) :
	ListSorter(listToUse)
{}

template<class T>
void ListSorterMerge<T>::sort()
{
	mergeSort(0, list.size() - 1);
}

template<class T>
void ListSorterMerge<T>::mergeSort(int first, int last)
{
	// base case -- empty list
	if (first >= last)
		return;

	int mid = (first + last) / 2;
	mergeSort(first, mid);
	mergeSort(mid + 1, last);
	merge(first, mid, last);
}

template<class T>
void ListSorterMerge<T>::merge(int first, int mid, int last)
{
	ListArray<T> temp(last - first + 1);

	// first subarray goes from first to mid
	int first1 = first;
	int last1 = mid;
	// second subarray goes from mid+1 to last
	int first2 = mid + 1;
	int last2 = last;

	// while both subarrays are not empty, copy the smaller item into the temp list
	while (first1 <= last1 && first2 <= last2)
		if (list.at(first1) <= list.at(first2))
			temp.push_back(list.at(first1++));
		else
			temp.push_back(list.at(first2++));

	// finish off the remaining bits of either the first or second subarray
	while (first1 <= last1)
		temp.push_back(list.at(first1++));
	while (first2 <= last2)
		temp.push_back(list.at(first2++));

	// copy the results back into the original array
	for (int i = 0; i < temp.size(); i++)
		list.at(first++) = temp.at(i);
}