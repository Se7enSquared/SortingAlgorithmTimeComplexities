#pragma once

#include "ListSorter.h"

template<class T>
class ListSorterBubble : public ListSorter<T>
{
public:
	ListSorterBubble(ListArray<T>& listToUse);
	virtual void sort();
};

template<class T>
ListSorterBubble<T>::ListSorterBubble(ListArray<T>& listToUse) :
	ListSorter(listToUse)
{}

template<class T>
void ListSorterBubble<T>::sort()
{
	for (int i = list.size() - 1; i > 0; i--)
		for (int j = 0; j < i; j++)
			if (list.at(j) > list.at(j + 1))
				std::swap(list.at(j), list.at(j + 1));
}