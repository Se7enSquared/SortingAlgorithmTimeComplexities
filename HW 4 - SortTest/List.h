#pragma once

/**
* Abstract base class for List ADT.
* Holds a list of values that can be appended to and indexed.
* Method names and functionality are loosely based on std::vector<T>
*
* @author Kevin Lundeen
* @version 1.0, 01/25/2016
*/
template<class T>
class List
{
public:
	/**
	 * Sole constructor takes a size hint. For this early version, we'll take
	 * this as a fixed-capacity list, so you can only append with push_back until
	 * you reach this initial capacity.
	 * @param sizeHint The initial capacity of the list
	 */
	List(int sizeHint);

	/**
	 * Returns whether this list is empty (i.e. whether its size is 0).
	 * @return true if this list has size() > 0, false otherwise
	 */
	virtual bool empty() const;

	/**
	 * Returns the number of elements in this list.
	 * This is the number of actual objects held in the list, which is not necessarily equal to its storage capacity.
	 * @return the number of elements
	 */
	virtual int size() const = 0;

	/**
	 * Returns the size of the storage space currently allocated for this list, expressed in terms of elements.
	 * This capacity is not necessarily equal to the list size. It can be equal or greater, with the extra space
	 * allowing to accommodate for growth.
	 * @return the maximum number of elements
	 */
	virtual int capacity() const = 0;

	/**
	 * Adds a new element at the end of this list, after its current last element. 
	 * @param newEntry The value to be appended to this list
	 * @return the index of the new entry (so that list.at(list.push_back(x)) == x)
	 * @throws out_of_range If the list does not have sufficient capacity for another element
	 */
	virtual int push_back(const T& newEntry) = 0;

	/**
	 * Returns a reference to the element at position n in the vector.
	 * This can then be used for read access or write access, e.g., <code>x = list.at(3)</code> 
	 * or <code>list.at(3) = 12</code>
	 * @param position Position of an element in this list (zero-based)
	 * @return The reference of the given element
	 * @throws out_of_range If the position is less than zero (undefined) or greater than the current list size
	 */
	virtual T& at(int position) = 0;
};

// constructor
template<class T>
List<T>::List(int sizeHint)
{}

// default implementation of empty method
template<class T>
bool List<T>::empty() const
{
	return size() == 0;
}