#include <iostream>
#include "SortTester.h"
#include "ListSorterBubble.h"
#include "ListSorterMerge.h"
#include "ListSorterInsertion.h"
#include "ListSorterQuick.h"
#include "ListSorterQuick2.h"

int main()
{
	using namespace std;
	cout << "======== QUICK ========" << endl;
		SortTester<ListSorterQuick<int>> quick;
			quick.runTests();
	cout << "======== QUICK2 ========" << endl;
		SortTester<ListSorterQuick2<int>> quick2;
			quick2.runTests();
	cout << "======== MERGE ========" << endl;
		SortTester<ListSorterMerge<int>> merge;
			merge.runTests();
	cout << "======== BUBBLE ========" << endl;
		SortTester<ListSorterBubble<int>> bubble;
			bubble.runTests();
	cout << "======== INSERTION ========" << endl;
		SortTester<ListSorterInsertion<int>> insertion;
			insertion.runTests();
	return 0;
}
